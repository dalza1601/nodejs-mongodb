'use stric';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// db.books.insert({ title: "Libro 1", author: "author 1", category: "category 1" })
// db.books.insert({ title: "Libro 2", author: "author 2", category: "category 2" })
// db.books.insert({ title: "Libro 3", author: "author 3", category: "category 3" })

var BookSchema = new Schema({
    title: String,
    author: String,
    category: String,
    // published: {
    //     type: Date,
    //     default: Date.now
    // },
    // keywords: Array,
    // published: Boolean,
    // Author: {
    //     type: Schema.ObjectId,
    //     ref: 'User'
    // },
    // detail: {
    //     modelNumber: Number,
    //     hardcoever: Boolean,
    //     reviews: Number,
    //     rank: Number,

    // }
})
module.exports = mongoose.model("Book", BookSchema);

