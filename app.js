var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var Book = require('./book.model');
var port = 8080;
var db = 'mongodb:localhost/library'

mongoose.connect(db, { useNewUrlParser: true });

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({
    extended: true
}));

app.get("/", function (req, res) {
    res.send('happy to be here');
});

app.get("/books", function (req, res) {
    console.log('getting all books');
    Book.find({})
        .exec(function (err, books) {

            if (err) {
                res.send("error has ocurred");
            } else {
                res.json(books);
            }

        });
});

app.get("/books/:id", function (req, res) {
    console.log('getting one book');

    Book.findOne({
        _id: req.params.id
    }).exec(function (err, book) {
        if (err) {
            res.send("error has ocurred");
        } else {
            res.json(book);
        }
    });

});

app.post("/book", function (req, res) {

    var newBook = Book();
    newBook.title = req.body.title;
    newBook.author = req.body.author;
    newBook.category = req.body.category;

    newBook.save(function (err, book) {

        if (err) {
            res.send("error saving book");
        } else {
            res.send(book);
        }
    });

});

app.put("/book/:id", function (req, res) {

    Book.findOneAndUpdate({
        _id: req.params.id
    }, { $set: { title: req.body.title } },
        { upsert: true },
        function (err, book) {
            if (err) {
                console.log("error update");
            } else {
                res.send(book);
                // res.status(204);
            }

        });

});

app.delete("/book/:id", function(req,res){

    Book.findOneAndRemove({
        _id: req.params.id
    }
        );
});

app.listen(port, function () {
    console.log('app listening on port:' + port);
});
